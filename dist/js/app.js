$(document).ready(function(){

//TABS
  $('.js-sizes-table td').mouseover(function(){
    var table = $(this).parents('table'),
        td = $(this).index(),
        tr = $(this).parents('tr').index();
    if (tr == 0 || td == 0) return;
    $(table.find('tr:first-child td')[td]).addClass('hover');
    $($(table.find('tr')[tr]).find('td')[0]).addClass('hover');
  }).mouseout(function(){
    var table = $(this).parents('table'),
        td = $(this).index(),
        tr = $(this).parents('tr').index();
    if (tr == 0 || td == 0) return;
    $(table.find('tr:first-child td')[td]).removeClass('hover');
    $($(table.find('tr')[tr]).find('td')[0]).removeClass('hover');
  });
  $('.js-tab-link').click(function(e){
    e.preventDefault();
    if ($(this).hasClass('active')) return;
    $(this).parents('.js-tabs-nav').find('.js-tab-link.active').removeClass('active');
    $(this).addClass('active');
    var tab = $('#'+$(this).attr('data-tab'))
    tab.parents('.js-tabs').find('.tab.active').removeClass('active');
    tab.addClass('active');
  });

//MODAL WINDOWS
  function openModal(modalId, isSmModal){
    $(modalId).addClass('modal_active modal_show')
              .on('animationend webkitAnimationEnd MSAnimationEnd oAnimationEnd', function(){
                $(this).removeClass('modal_show');
              });
    if (isSmModal){
      $('body').addClass('sm-modal-active');
      return;
    }
    $('body').addClass('modal-active');
  }

  function closeModal(modalId, isSmModal){
    var largeModalsCount = $('.modal_large.modal_active').length
        smModalsCount = $('.modal_sm.modal_active').length;
    $(modalId).attr('data-status', 'closing').addClass('modal_hide')
              .on('animationend webkitAnimationEnd MSAnimationEnd oAnimationEnd', function(){
                if ($(this).attr('data-status')=='closing'){
                  $(this).removeClass('modal_hide modal_active');
                  $(this).attr('data-status', '');
                }
              });
    if (isSmModal){
      if (smModalsCount == 1) $('body').removeClass('sm-modal-active');
      return;
    }
    if (largeModalsCount == 1) $('body').removeClass('modal-active');
  }

  $('.js-modal-open').click(function(e){
    e.preventDefault();
    var isSmModal = false;
    if ($($(this).attr('data-modal')).hasClass('modal_sm')) isSmModal = true;
    openModal($(this).attr('data-modal'), isSmModal);
  });

  $('.js-modal-close').click(function(e){
    e.preventDefault();
    var isSmModal = false;
    if ($(this).parents('.modal').hasClass('modal_sm')) isSmModal=true;
    closeModal('#'+$(this).parents('.modal').attr('id'), isSmModal);
  });

//FIXED MENU ON SCROLL EVENT
  $(window).scroll(function(){

    var scroll = $('html, body').scrollTop();
    alert(scroll);
    if (scroll>=$('.header').height()) {
      $('.menu-fixed').addClass('active');
      $('.sm-menu-btn').addClass('sm-menu-btn_dark');
    } else {
      $('.menu-fixed').removeClass('active');
      $('.sm-menu-btn').removeClass('sm-menu-btn_dark');
    }
  });


//CUSTOM SLIDER
  function carouselGoTo(slider, direction, index){
    var slideClassName = 'carousel__slide',
        slides = slider.children('.carousel__slides').children('.carousel__slide'),
        slidesCount = slides.length,
        slideCurrent = slider.children('.carousel__slides').children('.'+slideClassName+'_active'),
        slideCurrentIndex = 0,
        slideNextIndex = 0,
        slideNext = null,
        oldClassSuffix = '_old-left',
        newClassSuffix = '_new-right',
        paginationId = slider.attr('data-pagination');

    for (var i=0; i<slidesCount; i++){
      if ($(slides[i]).hasClass(slideClassName+'_active')){
        slideCurrentIndex = i;
      }
    }

    if (slideCurrentIndex==index) return;

    if (direction=='next'){
      slideNextIndex = slideCurrentIndex + 1 == slidesCount ? 0 : slideCurrentIndex + 1;
      slideNext = $(slides[slideNextIndex]);
      oldClassSuffix = '_old-left';
      newClassSuffix = '_new-right';
    }

    if (direction=='prev'){
      slideNextIndex = slideCurrentIndex - 1 < 0 ? slidesCount - 1 : slideCurrentIndex - 1;
      slideNext = $(slides[slideNextIndex]);
      oldClassSuffix = '_old-right';
      newClassSuffix = '_new-left';
    }

    if (direction=='index'){
      slideNextIndex = index || 0;
      if (slideNextIndex < 0 || slideNextIndex >= slidesCount) slideNextIndex = 0;
      slideNext = $(slides[slideNextIndex]);
      if (slideCurrentIndex < slideNextIndex) {
        oldClassSuffix = '_old-left';
        newClassSuffix = '_new-right';
      } else {
        oldClassSuffix = '_old-right';
        newClassSuffix = '_new-left';
      }
    }

    if(paginationId){
      $(paginationId).find('.js-carousel-btn.active').removeClass('active');
      $(paginationId).find('.js-carousel-btn[data-index="'+slideNextIndex+'"]').addClass('active');
    }

    slideCurrent.addClass(slideClassName+oldClassSuffix)
                .on('animationend webkitAnimationEnd MSAnimationEnd oAnimationEnd', function(){
                  $(this).removeClass(slideClassName+'_active '+slideClassName+oldClassSuffix);
                });
    slideNext.addClass(slideClassName+newClassSuffix+' '+slideClassName+'_new')
                .on('animationend webkitAnimationEnd MSAnimationEnd oAnimationEnd', function(){
                  $(this).removeClass(slideClassName+newClassSuffix+' '+slideClassName+'_new')
                         .addClass(slideClassName+'_active');
                });
  };

  $('.js-carousel-btn').click(function(e){
    e.preventDefault();

    var direction = $(this).attr('data-direction'),
        paginationId,
        slider = $(this).parents('.js-carousel');


    var paginationId = $(this).parents('.js-pagination').attr('id');
    if (paginationId){
      slider = $('.js-carousel[data-pagination="#'+paginationId+'"]');
    }

    if ($(this).parents('.carousel-arrows').length==1){
      slider = $($(this).parents('.js-carousel')[0]);
    }


    if (direction == 'index'){
      var index = parseInt($(this).attr('data-index'));
      carouselGoTo(slider, 'index', index);
    }
    if (direction == 'prev'){
      carouselGoTo(slider, 'prev');
    }
    if (direction == 'next'){
      carouselGoTo(slider, 'next');
    }

  });

  $.each($('.js-carousel'), function(i, k){
    var slides  = $(k).children('.carousel__slides').children('.carousel__slide').removeClass('carousel__slide_active');
    $(slides[0]).addClass('carousel__slide_active');
    $($($(k).attr('data-pagination')).find('.js-carousel-btn')[0]).addClass('active');


    if ($(k).attr('data-autoplay')){
      var interval = setInterval(function(){
        carouselGoTo($(k), 'next');
      },$(k).attr('data-autoplay'));

      var pagination = $($(k).attr('data-pagination'));
      pagination.find('.js-carousel-btn').click(function(){
        clearInterval(interval);
        interval = setInterval(function(){
          carouselGoTo($(k), 'next');
        },$(k).attr('data-autoplay'));
      });
    }
  });

  //swipe event for slider

  $('.js-carousel.carousel_fade-effect').swipe( {
    //Generic swipe handler for all directions
    swipeLeft:function(event, direction, distance, duration, fingerCount, fingerData) {
        carouselGoTo($(this), 'next');
    },
    swipeRight:function(event, direction, distance, duration, fingerCount, fingerData) {
        carouselGoTo($(this), 'prev');
    }
  });

  //Set some options later
  $("#test").swipe( {fingers:2} );


//MODAL TOOLTIPS
  $('.compare-table .tt').click(function(){
    openModalTooltip($(this));
  });
  function openModalTooltip(tooltip){
    $('#modal-tooltip .modal-tooltip__content').html(tooltip.find('.tt__content').html());
    openModal('#modal-tooltip', true);
  }



//WORKERS SLIDER ON MOBILE DEVICES
  var workersSliderDefined = false,
      workersSlider;
  function setWorkersSlider(){
    if(window.matchMedia('(max-width: 1024px)').matches)
    {
      if (!workersSliderDefined){
        workersSlider = $('.js-workers-slider').slick({
          infinite: true,
          slidesToShow: 2,
          slidesToScroll: 2,
          dots: true,
          responsive: [
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false
              }
            }
          ]
        });
        workersSliderDefined = true;
      }
    } else {
      if (workersSliderDefined){
        workersSlider.slick('unslick');
        workersSliderDefined = false;
      }
    }
  }

  $(window).resize(function(){
    setWorkersSlider();
  });

  setWorkersSlider();


//ANCHOR LINK
  $('.js-anchor').click(function(e){
    e.preventDefault();
    var target = $($(this).attr('href')),
        targetPos = target.offset().top;
    $('html, body').animate({scrollTop: targetPos},targetPos/5);
  });


//FORM SUBMIT INFO
  $('.form').submit(function(e){
    e.preventDefault();
    $(this).find('.form__result').addClass('active');
  });


//IMG-FILL PLUGIN
  $('.img-fill').imagefill();
});
