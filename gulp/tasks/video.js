var gulp     = require('gulp');
var cache    = require('gulp-cache');
var util     = require('gulp-util');
var imagemin = require('gulp-imagemin');
var config   = require('../config.js');


gulp.task('video', function(){
  return gulp
    .src([
      config.src.video + '**/*.*'
    ])
    .pipe(gulp.dest(config.dest.root))
});

gulp.task('video:watch', function() {
  gulp.watch(config.src.video + '**/*.*', ['video']);
});
